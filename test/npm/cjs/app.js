"use strict";

( async()=>{

  const dy = await import("dallmo-yaml");

  // locate the test config file
  const config_file = "./sample-config.yaml";

  // test read the config
  const config_obj = await dy.dallmo_yaml( config_file );
  console.log( config_obj );

})(); // self-run async main
