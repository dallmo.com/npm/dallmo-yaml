"use strict";

import {dallmo_yaml} from 'dallmo-yaml';

// locate the test config file
const config_file = "./sample-config.yaml";

// test read the config
const config_obj = await dallmo_yaml( config_file );
      console.log( config_obj );
