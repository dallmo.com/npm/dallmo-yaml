# dallmo-yaml

- a simple config reader based on `fs` and `js-yaml` ;
- config file assumed to be yaml ; 
- ESM-native
- using this in CJS has to be done via [dynamic import][ref-2] 

[ref-1]: https://redfin.engineering/node-modules-at-war-why-commonjs-and-es-modules-cant-get-along-9617135eeca1
[ref-2]: https://v8.dev/features/dynamic-import

# usage

## cjs
```
"use strict";

( async()=>{

  const dy = await import("dallmo-yaml");

  // locate the test config file
  const config_file = "./sample-config.yaml";

  // test read the config
  const config_obj = await dy.dallmo_yaml( config_file );
  console.log( config_obj );

})(); // self-run async main
```

## esm
```
"use strict";

import {dallmo_yaml} from 'dallmo-yaml';

// locate the test config file
const config_file = "./sample-config.yaml";

// test read the config
const config_obj = await dallmo_yaml( config_file );
      console.log( config_obj );
```

