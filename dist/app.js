"use strict";

import fs from 'fs';
import yaml from 'js-yaml';

//################################################################
async function dallmo_yaml( config_file ){

  // this is the obj storing the yarml config to be read
  let config_data_obj;

  try {
    const file_content = fs.readFileSync( config_file, 'utf8');
    config_data_obj = yaml.load( file_content );
  } catch (error) {
    const error_message = "error in reading yaml config : ";
    throw new( error_message ); 
    console.error( error.message );
  }; // try catch

    return config_data_obj;

}//function
//################################################################
// all exports go here
export {
  
  dallmo_yaml,

}; // export
